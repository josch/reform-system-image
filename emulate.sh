#!/bin/sh

qemu-system-aarch64 -netdev user,id=net0 -device virtio-net-pci,netdev=net0 \
	-device virtio-rng-pci,rng=rng0 -smp 4 -cpu host \
	-machine type=virt,gic-version=max,accel=kvm:tcg -no-user-config \
	-name debvm-run -m 1G \
	-drive media=disk,format=raw,discard=unmap,file=reform-system-imx8mq.img,if=virtio,cache=unsafe \
	-object rng-random,filename=/dev/urandom,id=rng0 -device virtio-gpu-pci \
	-device virtio-keyboard-pci -device virtio-tablet-pci \
	-kernel kernel -initrd initrd
