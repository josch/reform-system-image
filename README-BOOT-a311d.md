# A311D Bootloader build

## u-boot

```bash
git clone https://github.com/u-boot/u-boot
cd u-boot
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64

cp ../meson-g12b-bananapi-cm4-mnt-reform2.dts arch/arm/dts/
patch -p1 <../dts-makefile.patch
cp ../bananapi-cm4-mnt-reform2_defconfig configs/

make bananapi-cm4-mnt-reform2_defconfig
make -j8
```

This yields `u-boot.bin`.

## FIP

```bash
git clone https://github.com/libreelec/amlogic-boot-fip
cd amlogic-boot-fip
mkdir mnt-reform2-a311d
./build-fip.sh bananapi-cm4io ../u-boot/u-boot.bin mnt-reform2-a311d
```

This yields `mnt-reform2-a311d/u-boot.bin.sd.bin` et al.

## Flash

Replace sdX with target SD card.

```bash
dd if=mnt-reform2-a311d/u-boot.bin.sd.bin of=/dev/sdX conv=fsync,notrunc bs=512 skip=1 seek=1
# apparently not needed:
# dd if=mnt-reform2-a311d/u-boot.bin.sd.bin of=/dev/sdX conv=fsync,notrunc bs=1 count=440
```

See also:

- https://u-boot.readthedocs.io/en/latest/board/amlogic/bananapi-cm4io.html
- https://github.com/angerman/meson64-tools
