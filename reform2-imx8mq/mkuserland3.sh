#!/bin/bash
#
# This script installs extra applications that are not included in the minimal rescue image.
#

set -e

ETC=./template-etc
SKEL=./template-skel

mmdebstrap \
	--architectures=arm64 \
	--variant=custom \
	--setup-hook='mmtarfilter "--path-exclude=/dev/*" < target-userland.tar | tar -C "$1" -x' \
	${comment#set up motd} \
	--customize-hook='rm -f "$1"/etc/motd' \
	--customize-hook='ln -s motd-full "$1"/etc/motd' \
	${comment#install applications} \
	--customize-hook='mv "$1"/etc/apt/apt.conf.d/10apt-listbugs "$1"/etc/apt/apt.conf.d/10apt-listbugs.bak' \
	--customize-hook='chroot "$1" apt-get install --yes git libreoffice libreoffice-gtk3 inkscape firefox-esr chromium emacs gimp wmaker x11-utils imagemagick-6.q16' \
	--customize-hook='chroot "$1" apt-get install --yes evolution freecad ardour sxiv minetest neverball scummvm dosbox wf-recorder wev linphone-desktop kicad' \
	${comment#install patched software from mntre} \
	--customize-hook='chroot "$1" apt-get install --yes cage wayvnc ffmpeg' \
	${comment#install a minimal gnome desktop} \
	--customize-hook='chroot "$1" apt-get install --yes --no-install-recommends gnome-control-center gnome-session openjdk-11-jre-headless gnome-disk-utility gnome-icon-theme breeze-icon-theme gnome-system-monitor gnome-settings-daemon mpv eog evince gedit thunar pavucontrol grim fonts-inter fonts-noto-color-emoji pulseaudio unicode-data engrampa rofi slurp arc-theme' \
	${comment#install kde plasma} \
	--customize-hook='chroot "$1" apt-get install --yes plasma-workspace-wayland kde-plasma-desktop plasma-nm systemsettings powerdevil' \
	--customize-hook='chroot "$1" apt-get install --yes --no-install-recommends plasma-discover apt-config-icons-large' \
	--customize-hook='mv "$1"/etc/apt/apt.conf.d/10apt-listbugs.bak "$1"/etc/apt/apt.conf.d/10apt-listbugs' \
	--customize-hook='rm "$1"/etc/resolv.conf' \
	${comment#workaround for minetest camera-in-head bug https://github.com/minetest/minetest/issues/11987 fixed in mesa 22.0} \
	--customize-hook='chroot "$1" mogrify -fill black -colorize 255 -transparent black /usr/share/games/minetest/games/minetest_game/mods/player_api/models/character.png' \
	${comment#remove broken gnome online accounts panel} \
	--customize-hook='rm "$1"/usr/share/applications/gnome-online-accounts-panel.desktop' \
	--customize-hook='rm "$1"/etc/ssl/certs/java/cacerts' ${comment#regenerate with update-ca-certificates -f} \
	'' target-userland-full.tar
