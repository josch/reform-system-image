#!/bin/bash
# Copyright 2018-2023 Lukas F. Hartmann / MNT Research GmbH
# Copyright 2021-2023 Johannes Schauer Marin Rodrigues <josch@mister-muffin.de>
# SPDX-License-Identifier: GPL-3.0-only

# shellcheck disable=SC2016 # Intentional quoting technique

set -eu

export LC_ALL=C.UTF-8

: "${DIST:=unstable}"
: "${MIRROR:=mntre.com}"

nth_arg() {
	shift "$1"
	printf "%s" "$1"
}

usage() {
	echo "usage: $0 [-m mirror] [-d dist] [board,...]" >&2
	exit 1
}

usage_error() {
	echo "error: $*" 1>&2
	usage
}

while getopts :h:d:m:-: OPTCHAR; do
	case "$OPTCHAR" in
		d) DIST="$OPTARG"   ;;
		m) MIRROR="$OPTARG" ;;
		h) usage            ;;
		-)	case "$OPTARG" in
				help)     usage                                                    ;;
				dist)     DIST="$(nth_arg "$OPTIND" "$@")";   OPTIND=$((OPTIND+1)) ;;
				dist=*)   DIST="${OPTARG#*=}"                                      ;;
				mirror)   MIRROR="$(nth_arg "$OPTIND" "$@")"; OPTIND=$((OPTIND+1)) ;;
				mirror=*) MIRROR="${OPTARG#*=}"                                    ;;
				*)        echo "unrecognized option --$OPTARG">&2; exit 1          ;;
			esac ;;
		:)   usage_error "missing argument for -$OPTARG" ;;
		'?') usage_error "unrecognized option -$OPTARG"  ;;
		*)   echo "internal error while parsing command options">&2; exit 1;;
	esac
done
shift "$((OPTIND - 1))"

SYSIMAGES="reform-system-imx8mq reform-system-ls1028a"
if [ "$#" -eq 0 ]; then
	case "$DIST" in
		bookworm)           : ;;
		bookworm-backports) SYSIMAGES="$SYSIMAGES reform-system-a311d";;
		testing)            SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp";;
		unstable)           SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp";;
		experimental)       SYSIMAGES="$SYSIMAGES reform-system-a311d pocket-reform-system-a311d pocket-reform-system-imx8mp reform-system-imx8mp reform-system-rk3588";;
		*) echo "unsupported distribution: $DIST" >&2; exit 1;;
	esac
else
	SYSIMAGES="$*"
fi

case "$MIRROR" in
	reform.debian.net)
		case "$DIST" in bookworm|bookworm-backports) : ;; *)
			echo "reform.d.n only supports bookworm and bookworm-backports" >&2
			exit 1
		esac
		;;
	mntre.com)
		case "$DIST" in testing|unstable|experimental) : ;; *)
			echo "mntre.com only supports testing, unstable and experimental" >&2
			exit 1
		esac
		;;
	*) echo "unsupported mirror: $MIRROR" >&2; exit 1;;
esac

# make sure build tools are installed
set -- mmdebstrap e2fsprogs git mount parted python3-apt
if [ "$(dpkg --print-architecture)" != arm64 ]; then
	set -- "$@" arch-test qemu-user-static
fi
if [ "$(dpkg-query --showformat '${db:Status-Status}\n' --show "$@" | sort -u)" != "installed" ]; then
	echo "Not all dependencies of this script are installed."
	echo "Run the following command to install them:"
	echo
	echo "    sudo apt install " "$@"
	exit 1
fi
if dpkg --compare-versions "$(dpkg-query --showformat='${Version}\n' --show mmdebstrap)" lt "0.8.4-1"; then
	echo "mmdebstrap version must be >= 0.8.4-1"
	exit 1
fi

# if we are in a git repository and if SOURCE_DATE_EPOCH is not set, use the
# timestamp of the latest git commit
if [ -z ${SOURCE_DATE_EPOCH+x} ] && git -C . rev-parse 2>/dev/null; then
	SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)
else
	SOURCE_DATE_EPOCH=$(date +%s)
fi
export SOURCE_DATE_EPOCH

# /tmp might be too small for the full system, so we set a different TMPDIR
TMPDIR="$(pwd)"
export TMPDIR

# We need a separate partition for /boot for two reasons:
#
# 1) To boot encrypted NVMe
# 2) If we boot a system that is not on the SD-Card (even unencrypted) then we
#    need to mount the partition containing kernel, initrd and dtb. If /boot is
#    not in its own partition, then mounting that partition to /boot will
#    result in the files being in /boot/boot. So we need to create a partition
#    where kernel, initrd and dtb are at the root of the partition. Otherwise
#    we cannot upgrade the kernel from the booted system.

# debian-installer chooses 999424 sectors (= 488MB) by default
BOOTSIZE=488
ROOTSIZE=4096

rm -f ./machines/*.conf
mkdir -p ./machines ./repo

# create Packages and Release file for custom repo
if [ -n "$(find repo -maxdepth 1 -name "*.deb" -print -quit)" ]; then
	echo "I: Creating repository from *.deb in ./repo and serving via HTTP" >&2
	env --chdir=./repo apt-ftparchive packages . > ./repo/Packages
	env --chdir=./repo apt-ftparchive -o APT::FTPArchive::Release::Codename=reform-local-repo -o APT::FTPArchive::Release::Label=reform-local-repo release . > ./repo/Release
	# use setpriv to automatically send TERM when this script exits
	setpriv --pdeathsig TERM python3 -m http.server -d repo &
else
	echo "I: No *.deb files found in ./repo" >&2
fi

# build the debian userland and configure it

# fill $@ array with options passed to mmdebstrap
set -- --architectures=arm64 --components=main,non-free-firmware --variant="minbase" --verbose
# packages for graphical user interface
set -- "$@" --include="xwayland xterm sway fonts-inter fonts-noto-color-emoji waybar swayidle swaylock mesa-utils lxpolkit wayland-protocols wofi wireplumber papirus-icon-theme libglib2.0-bin gsettings-desktop-schemas gnome-disk-utility gnome-themes-extra-data gnome-icon-theme gnome-settings-daemon gnome-system-monitor grim slurp gedit evince mpv sxiv gvfs-backends unicode-data engrampa neverball minetest qt5ct kde-style-breeze dunst pasystray wev wf-recorder wayvnc"
# reform-tools recommends
set -- "$@" --include=" brightnessctl foot gir1.2-ayatanaappindicator3-0.1 gir1.2-notify-0.7 gnome-system-monitor ircii pavucontrol pkexec pulseaudio-utils python3 python3-gi synaptic thunar x11-xserver-utils fonts-font-awesome"
case "$DIST" in
	unstable|experimental) set -- "$@" --include="wayfire reform-firedecor firefox";;
	*)                     set -- "$@" --include="firefox-esr"
esac
# packages for networking
set -- "$@" --include="iproute2 iptables inetutils-ping elinks isc-dhcp-client netcat-traditional net-tools network-manager network-manager-gnome nfacct ntp ntpdate rsync telnet traceroute wpasupplicant curl wget w3m rfkill ifupdown netbase openssh-client blueman firmware-realtek"
# packages for system administration
set -- "$@" --include="apt apt-utils apt-listbugs apt-file cron cryptsetup lvm2 dbus-bin e2fsprogs fbset init-system-helpers ncdu parted pciutils policykit-1 procps sudo systemd systemd-sysv tmux u-boot-tools screen"
# utilities
set -- "$@" --include="busybox console-data console-setup cpio file flash-kernel gnupg gpgv htop kbd lm-sensors readline-common usbutils xdg-utils bsdmainutils less nano micro vim alsa-utils dosfstools python3-psutil reform-handbook"
# miscellaneous
set -- "$@" --include="brightnessctl brightness-udev ca-certificates debian-archive-keyring dialog gpm ncurses-term locales bash-completion man-db cryptsetup-initramfs linux-image-arm64 linux-headers-arm64 reform-tools gpiod"
# apt preferences
set -- "$@" --setup-hook='mkdir -p "$1"/etc/apt/sources.list.d'
case "$MIRROR" in
	mntre.com)
		set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: release n=reform, l=reform"; echo "Pin-Priority: 990"; } > "$1"/etc/apt/preferences.d/reform.pref' \
			--setup-hook='copy-in mntre.sources /etc/apt/sources.list.d/'
		;;
	reform.debian.net)
		set -- "$@" --setup-hook='{ echo "Package: *"; echo "Pin: origin \"reform.debian.net\""; echo "Pin-Priority: 999"; } > "$1"/etc/apt/preferences.d/reform.pref' \
			--setup-hook='copy-in reform_bookworm.sources /etc/apt/sources.list.d/'
		;;
esac
case "$DIST" in
	bookworm-backports) set -- "$@" --setup-hook='copy-in reform_bookworm-backports.sources /etc/apt/sources.list.d/';;
	experimental) set -- "$@" --setup-hook='echo deb http://deb.debian.org/debian experimental main > "$1"/etc/apt/sources.list.d/experimental.list';;
esac
# copy out machine descriptions from reform-tools so that we can use them as well
set -- "$@" --customize-hook="sync-out /usr/share/reform-tools/machines ./machines"
# list packages and versions that are not from debian.org
set -- "$@" --customize-hook="APT_CONFIG=\$MMDEBSTRAP_APT_CONFIG python3 -c 'import apt_pkg;apt_pkg.init();cache=apt_pkg.Cache(None);print(\"\n\".join(sorted([f\"{pkg.name:<44} {pkg.current_ver.ver_str}\" for pkg in cache.packages if pkg.current_ver for pfile, _ in pkg.current_ver.file_list if not pfile.not_source and not pfile.site.endswith(\".debian.org\")])))' > \"\$1\"/custom-pkgs.lst" \
		--customize-hook='copy-out /custom-pkgs.lst .' \
		--customize-hook='rm "$1"/custom-pkgs.lst'
# set up local repo if necessary
if [ -n "$(find repo -maxdepth 1 -name "*.deb" -print -quit)" ]; then
	# use Pin-Priority: 1000 to install from this repo even if it would
	# cause a downgrade
	set -- "$@" \
		--setup-hook='{ echo "Package: *"; echo "Pin: release n=reform-local-repo, l=reform-local-repo"; echo "Pin-Priority: 1000"; } > "$1"/etc/apt/preferences.d/reform-local-repo.pref' \
		--customize-hook='rm "$1"/etc/apt/preferences.d/reform-local-repo.pref' \
		--setup-hook='echo "deb [arch=arm64 trusted=yes] http://127.0.0.1:8000/ ./" > "$1"/etc/apt/sources.list.d/reform-local-repo.list' \
		--customize-hook='rm "$1"/etc/apt/sources.list.d/reform-local-repo.list'
fi
# The board specific kernel commandline arguments now live in
# /usr/share/flash-kernel/ubootenv.d/00reform2_ubootenv which does the
# platform detection and then adds the options to ${bootargs}.
# We still want to overwrite /etc/default/flash-kernel because by default it will contain
# LINUX_KERNEL_CMDLINE="quiet" and we don't want that
set -- "$@" --essential-hook='{ echo LINUX_KERNEL_CMDLINE=\"\"; echo LINUX_KERNEL_CMDLINE_DEFAULTS=\"\"; } > "$1"/etc/default/flash-kernel'
# select timezone and locales
# FIXME: this should be done by a firstboot script
set -- "$@" \
	--essential-hook='echo tzdata tzdata/Areas select Europe | chroot "$1" debconf-set-selections' \
	--essential-hook='echo tzdata tzdata/Zones/Europe select Berlin | chroot "$1" debconf-set-selections' \
	--essential-hook='echo locales locales/default_environment_locale select en_US.UTF-8 | chroot "$1" debconf-set-selections' \
	--essential-hook='echo locales locales/locales_to_be_generated multiselect en_US.UTF-8 UTF-8 | chroot "$1" debconf-set-selections'
# set up motd
set -- "$@" --customize-hook='rm -f "$1"/etc/motd' --customize-hook='ln -s motd-full "$1"/etc/motd'
# populate root user and skel
set -- "$@" \
	--customize-hook='echo '"'"'if [ "$(whoami)" = "root" ]; then cat /etc/reform-root-help; elif [ -z "$WAYLAND_DISPLAY" ]; then cat /etc/reform-help; fi'"'"' >> "$1"/etc/skel/.profile' \
	--customize-hook='chroot "$1" sh -c "rsync -Pha /etc/skel/ /root"'
# populate /etc
set -- "$@" \
	--essential-hook='{ echo LABEL=reformsdroot / auto errors=remount-ro 0 1; echo LABEL=reformsdboot /boot auto errors=remount-ro 0 1; } > "$1"/etc/fstab' \
	--customize-hook='echo reform > "$1"/etc/hostname' \
	--customize-hook='{ echo 127.0.0.1 localhost reform; echo ::1 localhost ip6-localhost ip6-loopback reform; echo ff02::1 ip6-allnodes; echo ff02::2 ip6-allrouters; } > "$1"/etc/hosts'
# or else EXTRA_GROUPS doesnt work?
set -- "$@" \
	--customize-hook='sed -i "s/^#EXTRA_GROUPS=.*/EXTRA_GROUPS=audio cdrom dip floppy video plugdev netdev/" "$1"/etc/adduser.conf' \
	--customize-hook='sed -i "s/^#ADD_EXTRA_GROUPS=.*/ADD_EXTRA_GROUPS=1/" "$1"/etc/adduser.conf'
# remove root password -- using `passwd -d root` produces unreproducible output
set -- "$@" \
	--customize-hook='echo "root:root" | chroot "$1" chpasswd' \
	--customize-hook='chroot "$1" sed -i "s/^root:[^:]\+:/root::/" /etc/shadow' \
	--customize-hook='rm "$1"/etc/resolv.conf'
# copy out kernel and initrd for use with qemu
set -- "$@" --customize-hook="download /vmlinuz kernel" --customize-hook="download /initrd.img initrd"
# copy out contents of /boot
set -- "$@" --customize-hook="tar-out /boot boot.tar"
# choose the correct mode
if [ "$(id -u)" = 0 ]; then
	set -- "$@" --mode=root
else
	set -- "$@" --mode=unshare
fi

case "$DIST" in
	testing)
		set -- "$@" testing
		;;
	unstable|experimental)
		set -- "$@" unstable
		;;
	bookworm|bookworm-backports)
		set -- "$@" bookworm
		;;
esac

if [ "$(id -u)" = 0 ]; then
	run_inside_userns() {
		"$@"
	}
else
	run_inside_userns() {
		unshare --user --map-auto --map-user=65536 --map-group=65536 --keep-caps -- "$@"
	}
fi

TEMPROOT=
cleanup() {
	if test -n "$TEMPROOT"; then
		run_inside_userns rm -Rf "$TEMPROOT"
	fi
	rm -f boot.ext4 boot.tar root.ext4
}
trap cleanup EXIT
# The default action for these signals does not invoke the EXIT trap.
trap 'exit 1' HUP INT QUIT TERM
TEMPROOT="$(mktemp -d)"

mmdebstrap "$@" --format=directory --mode=unshare --skip=output/dev "$TEMPROOT"

# create a common root partition from the above
# this will be identical independent of the sysimage
run_inside_userns find "$TEMPROOT/boot/" -mindepth 1 -delete
: >root.ext4
run_inside_userns /sbin/mkfs.ext4 -L reformsdroot -d "$TEMPROOT" "root.ext4" "${ROOTSIZE}M"

# system ---------------------------------------------------------

# remove MNT Reform 2 HDMI.conf because we do not create a sysimage for it
# and it would produce multiple matches for reform-system-imx8mq
rm "machines/MNT Reform 2 HDMI.conf"

for SYSIMAGE in $SYSIMAGES; do
	CONF="$(grep -l "^SYSIMAGE=\"$SYSIMAGE\"\$" machines/*.conf)";
	if [ ! -e "$CONF" ]; then
		echo "reform-tools does not know about a configuration for $SYSIMAGE" >&2
		exit 1
	fi
	# shellcheck source=/dev/null
	. "$CONF"
	case "$SYSIMAGE" in
		reform-system-imx8mq)  PROJECT="reform-boundary-uboot"; TAG="2023-10-18"; SHA1="3de987054676b65b80ecb65455465326c9539928";;
		reform-system-a311d)   PROJECT="reform-a311d-uboot";    TAG="2023-10-18"; SHA1="db5f2efb62da58dbf548e73a97764cc1380ed4ab";;
		reform-system-ls1028a) PROJECT="reform-ls1028a-uboot";  TAG="2023-10-10"; SHA1="41570d83a25e8fb6ec84afa1e4cc9559c962d621";;
		pocket-reform-system-a311d)
			/usr/lib/apt/apt-helper download-file "https://source.mnt.re/reform/reform-a311d-uboot/-/jobs/artifacts/main/raw/meson-g12b-bananapi-cm4-mnt-pocket-reform-flash.bin?job=build" "$(basename "$DTBPATH" .dtb)-flash.bin"
			unset DTBPATH
			continue
			;;
		pocket-reform-system-imx8mp)
			/usr/lib/apt/apt-helper download-file "https://source.mnt.re/reform/reform-imx8mp-uboot/-/jobs/artifacts/main/raw/imx8mp-mnt-pocket-reform-flash.bin?job=build" "$(basename "$DTBPATH" .dtb)-flash.bin"
			unset DTBPATH
			continue
			;;
		reform-system-imx8mp)
			/usr/lib/apt/apt-helper download-file "https://source.mnt.re/reform/reform-imx8mp-uboot/-/jobs/artifacts/main/raw/imx8mp-mnt-reform2-flash.bin?job=build" "$(basename "$DTBPATH" .dtb)-flash.bin"
			unset DTBPATH
			continue
			;;
		reform-system-rk3588)
			echo "FIXME: no u-boot yet" >&2
			exit 1
			;;
	esac
	/usr/lib/apt/apt-helper download-file \
		"https://source.mnt.re/reform/$PROJECT/-/jobs/artifacts/$TAG/raw/flash.bin?job=build" \
		"$(basename "$DTBPATH" .dtb)-flash.bin" "SHA1:$SHA1"
	unset DTBPATH
done

# download DisplayPort firmware blob for LS1028A
/usr/lib/apt/apt-helper download-file \
	"https://source.mnt.re/reform/reform-ls1028a-uboot/-/raw/main/ls1028a-mhdpfw.bin" \
	ls1028a-mhdpfw.bin "SHA1:fa96b9aa59d7c1e9e6ee1c0375d0bcc8f8e5b78c"


for SYSIMAGE in $SYSIMAGES; do
	# invoke mmdebstrap again to produce a platform specific /boot
	# fill $@ array with options passed to mmdebstrap
	CONF="$(grep -l "^SYSIMAGE=\"$SYSIMAGE\"\$" machines/*.conf)";
	if [ ! -e "$CONF" ]; then
		echo "reform-tools does not know about a configuration for $SYSIMAGE" >&2
		exit 1
	fi
	# shellcheck source=/dev/null
	. "$CONF"
	if [ -z ${DTBPATH+x} ]; then
		echo "machine configuration for $SYSIMAGE misses DTBPATH" >&2
		exit 1
	fi
	if [ -z ${UBOOT_OFFSET+x} ]; then
		echo "machine configuration for $SYSIMAGE misses UBOOT_OFFSET" >&2
		exit 1
	fi
	if [ -z ${FLASHBIN_OFFSET+x} ]; then
		echo "machine configuration for $SYSIMAGE misses FLASHBIN_OFFSET" >&2
		exit 1
	fi
	MACHINE="$(basename "$CONF" .conf)"
	set -- --variant=custom --skip=update,setup,cleanup \
		--skip=check/empty \
		--setup-hook="tar-in boot.tar /" \
		--customize-hook="upload ./$(basename "$DTBPATH" .dtb)-flash.bin /boot/flash.bin" \
		--chrooted-customize-hook="
			set -exu;
			awk \"/^Machine: $MACHINE\$/,/^\$/\" /usr/share/flash-kernel/db/all.db >&2
			awk \"/^Machine: $MACHINE\$/,/^\$/\" /usr/share/flash-kernel/db/all.db | grep --silent \"^DTB-Id: $DTBPATH\$\" >&2;
			echo \"$MACHINE\" > /etc/flash-kernel/machine;
			echo yes > /etc/flash-kernel/ignore-efi;
			chown -R root:root /boot/flash.bin;
			flash-kernel;
			readlink /boot/dtb >&2;
			readlink /boot/dtb-*reform2-arm64 >&2;
			find /boot/ -wholename \"/boot/dtbs/*/$DTBPATH\" >&2;
			find /boot/ -wholename \"/boot/dtbs/*/$DTBPATH\" | grep --silent \"/$DTBPATH\$\";
			test -e \"\$(find /boot/ -wholename \"/boot/dtbs/*/$DTBPATH\")\";
			"
	case "$SYSIMAGE" in
		reform-system-ls1028a)
			set -- "$@" \
				--customize-hook='upload ls1028a-mhdpfw.bin /boot/ls1028a-mhdpfw.bin' \
				--chrooted-customize-hook='update-initramfs -u'
			;;
	esac
	mmdebstrap "$@" '' --format=directory --mode=unshare --skip=output/dev "$TEMPROOT"

	: >boot.ext4
	run_inside_userns /sbin/mkfs.ext4 -L reformsdboot -d "$TEMPROOT/boot" "boot.ext4" "${BOOTSIZE}M"

	# make sure that the filesystem includes boot.scr
	/usr/sbin/debugfs boot.ext4 -R "stat boot.scr" \
		| sed 's/ \+/ /g' | grep --quiet "Type: regular Mode: 0644 Flags: 0x"

	python3 sparsedd.py root.ext4 "$SYSIMAGE.img" "$(((BOOTSIZE+4)*1024*1024))"
	python3 sparsedd.py boot.ext4 "$SYSIMAGE.img" 4194304
	truncate --size="+512" "$SYSIMAGE.img"
	/sbin/parted -s "$SYSIMAGE.img" "mklabel msdos"
	# reproducible disk signature
	printf mntr | dd of="$SYSIMAGE.img" seek=440 bs=1 conv=notrunc
	/sbin/parted -s "$SYSIMAGE.img" "mkpart primary ext4 4MiB $((BOOTSIZE+4))MiB"
	/sbin/parted -s "$SYSIMAGE.img" "mkpart primary ext4 $((BOOTSIZE+4))MiB $((BOOTSIZE+ROOTSIZE+4))MiB"
	/sbin/parted -s "$SYSIMAGE.img" print

	# install u-boot
	dd if="./$(basename "$DTBPATH" .dtb)-flash.bin" of="$SYSIMAGE.img" conv=notrunc bs=512 seek="$((UBOOT_OFFSET/512))" skip="$((FLASHBIN_OFFSET/512))"

	unset DTBPATH
	unset UBOOT_OFFSET
	unset FLASHBIN_OFFSET
done

echo "Custom packages not from debian.org:" >&2
cat custom-pkgs.lst >&2
echo >&2

echo "MD5 checksums:" >&2
for SYSIMAGE in $SYSIMAGES; do
	md5sum "$SYSIMAGE.img" >&2
done
