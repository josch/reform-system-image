# MNT Reform System Image

This is a collection of scripts used to build the MNT Reform System Images, which are based on Debian GNU/Linux. The images can be transferred to an SD card and booted by an MNT Reform computer.

## Downloading and Flashing a System Image

Most users will want to download a pre-built image from: https://mnt.re/system-image

1. From the link above, download the file whose name matches the Processor Module in your MNT Reform:
  - NXP i.MX8MQ Processor: `reform-system-imx8mq.img.gz`
  - RCM4 with BPi CM4/A311D Processor: `reform-system-a311d.img.gz`
  - NXP LS1028A Processor: `reform-system-ls1028a.img.gz`

2. Unzip the image using `gunzip reform-system-imx8mq.img.gz` (Substitute your actual image file name, also in the following steps)

3. Identify the device name of your SD card. For example, you can check the output of `lsblk` before and after you insert the card and compare which new device appears. Below we use the device `sdx`, but it will probably be a different one on your computer.

4. Copy the image to your SD card using `sudo dd if=reform-system-imx8mq.img of=/dev/sdx bs=8M status=progress`.

## Advanced: Grow the Root Partition of the Flashed Image

You can extend the size of the second partition to use the full size of your SD card using the following commands (again substitute the correct device name):

```
sudo growpart /dev/sdx 2
sudo e2fsck -f /dev/sdx2
sudo resize2fs /dev/sdx2
```

## How to Build the Images

Building the images requires considerable disk space and CPU resources, it is generally only required if you want to contribute to the development of the system images.

To create a system image for all platforms, run this:

    ./mkimage.sh

To only create the image for a specific platform, choose one out of imx8mq,
a311d and ls1028a like so:

    ./mkimage.sh imx8mq

The resulting file is named `reform-system-${PLATFORM}.img` where `${PLATFORM}`
is the name of the platform the image was built for (one of imx8mq, a311d or
ls1028a).

## License

Copyright 2018-2020 Lukas F. Hartmann / MNT Research GmbH

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
