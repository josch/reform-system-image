#!/usr/bin/env python

import subprocess
import os
import sys


def main():
    infd = os.open(sys.argv[1], os.O_RDONLY)
    inlength = os.lseek(infd, 0, os.SEEK_END)
    outfd = os.open(sys.argv[2], os.O_CREAT | os.O_WRONLY)
    outlength = os.lseek(outfd, 0, os.SEEK_END)
    offset = int(sys.argv[3])
    curr = 0
    while True:
        try:
            data = os.lseek(infd, curr, os.SEEK_DATA)
        except OSError:
            # no more data
            break
        try:
            hole = os.lseek(infd, data, os.SEEK_HOLE)
        except OSError:
            # no hole afterwards, copy until EOF
            hole = inlength
        print(
            f"copying range {data}..{hole} ({100*hole/inlength:.2f}%)", file=sys.stderr
        )
        os.copy_file_range(
            infd, outfd, hole - data, offset_src=data, offset_dst=data + offset
        )
        curr = hole
    if outlength < inlength + offset:
        os.truncate(outfd, inlength + offset)
    os.close(infd)
    os.close(outfd)


if __name__ == "__main__":
    main()
